FROM ubuntu:18.04

WORKDIR /audit_application 
COPY . /audit_application

RUN apt-get update && apt-get install -y cmake build-essential gcc git wget curl
RUN apt-get update && apt-get install -y python3 python3-pip python3-setuptools

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update -y
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql17

RUN apt-get install -y unixodbc unixodbc-dev

#RUN chmod -r 777 audit_application/app.py

RUN pip3 install -r requirements.txt

EXPOSE 5000
CMD [ "python3","app.py" ]

