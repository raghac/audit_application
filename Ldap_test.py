from ldap3 import Connection, Server, ALL
import json
from flask import Flask, Blueprint, render_template, session, redirect, request, make_response, url_for, flash
from .forms import LoginForm
from functools import wraps
from flask.wrappers import Response
app = Flask(__name__) 
ldsp_server = f"ldap://ldapap.corp.amvescap.net:389"
root_dn = "dc=corp,dc=amvescap,dc=net"
server = Server(ldsp_server, get_info=ALL)

def login_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        cookie = request.cookies.get('userID')
        auth = False
        if cookie or 'user' in session:
            user = cookie if cookie else session['user']
            # auth = get_user_from_db(user)  #check user in database before every endpoint.
            if user:
                return fn(user, *args, **kwargs)
            else:
                # flash("Session exists, but user does not exist (anymore)")
                res = make_response(redirect(url_for('auth.login')))
                res.set_cookie('userID', user, max_age=0)
                return res
        else:
            return redirect(url_for('auth.login'))
    return wrapper


def authenticate_user(username, password):
    ad_group_member = False
    l_success_msg = 'unsuccess'
    allowed_groups = ['CN=*IVZ- All Technology Only']
    user = "CORP\\" + username
    print('Username for LDAP: ', user)
    connection = Connection(server,
                        user=user,
                        password=password)
    if not connection.bind():
        print(f" *** Cannot bind to ldap server: {connection.last_error} ")
        l_success_msg = f' ** Failed Authentication: {connection.last_error}'
    else:
        print(f" *** Successful bind to ldap server")
        l_search = connection.search(root_dn, f'(&(objectclass=user)(sAMAccountName={username}))', attributes=['cn','mail','sn','givenName','employeeID', 'memberOf'])
        response = json.loads(connection.response_to_json())
        groups = response['entries'][0]['attributes']['memberOf']
        l_success_msg = 'Success'
        for group in groups:
            grp = group.split(',')
            if grp[0] in allowed_groups:
                ad_group_member = True
                return l_success_msg, ad_group_member, response['entries'][0]['attributes']

    return l_success_msg, ad_group_member, {}

##App routings from app.py
@app.route('/', methods =["GET","POST"])
def index():
    if request.cookies.get('auth_token'):
        session['auth_token']=request.cookies.get('auth_token')
        session['audited_by']=request.cookies.get('audited_by')
        session['logged_in']=request.cookies.get('logged_in')
        return redirect(url_for('audit_form'))
    return redirect(url_for('login'))   
    #192.168.29.164
    # return redirect('http://192.168.29.164:5000/login'),302
@app.route("/login")
def login():
    form = LoginForm()
    try:
        if 'user' in session:
            return redirect('/site/home')
        else:
            return render_template('auth/login.html', title='Sign In', form=form)
    except Exception as e:
        raise e
    
@app.route('/login',methods=['POST','GET'])
def login():
    return render_template('login.html')  
    
@app.route('/logout',methods=["GET","POST"])
def logout():
    resp=make_response(redirect(url_for('index')))
    # resp.delete_cookie('auth_token')
    resp.set_cookie('auth_token', '', expires=0)
    resp.set_cookie('audited_by', '', expires=0)
    session.pop('auth_token')
    session.pop('audited_by')
    session.pop('logged_in')
    # print(session['auth_token'])
    # session.clear()
    return resp

@app.route('/authenticate', methods = ['POST'])
def authenticate():
    try:
        form = LoginForm()
        if request.method == 'POST':
            authenticate = False
            username=request.form["corpid"]
            password=request.form['password']
            session['auth_token'] = username
            #result=check_user(username,password)
            #username = form.username.data
            #password = form.password.data
            #cookie = form.remember_me.data
            response, ad_grp_member, data = authenticate_user(username.lower(), password)
            print(response)
            if response == 'Success':
                if ad_grp_member:
                    matched = match_user_profile(username.lower())
                    if matched == False:
                        user_id = data.get('cn')
                        fullname = data.get('givenName')+' '+data.get('sn')
                        emp_id = data.get('employeeID')
                        email = data.get('mail')
                        # created_date = datetime.now()
                        emp_data = (user_id, fullname, emp_id, email)
                        add_user_to_db(emp_data)
                    resp = make_response(redirect('/site/home'))
                    session['user'] = username
                    if cookie is True:
                        print('Setting Cookie')
                        resp.set_cookie('userID', username, max_age= 2592000)
                    return resp
                else:
                    flash("User is not present in allowed group")
                    return redirect(url_for('auth.login'))
            else:
                flash("Invalid Credentials!")
                return redirect(url_for('auth.login'))

    except Exception as e:
        raise e


@app.route('/home',methods=["POST"])
def home_page():
    if(request.method == "POST"):
        username=request.form["corpid"]
        password=request.form['password']
        session['auth_token'] = username
        result=check_user(username,password)
        session['otp']=0
        if(result and password=='Invesco@123'):
            session['otp']=generateOTP()
            send_mail(result[0]['email'],'http://usappvisilt101:5004/set_password/'+username,session['otp'])
            return render_template('password.html',message='sent mail')  
        if(result):
            audited=result[0]['u_ad_display_name']
            session['audited_by']=audited
            resp = make_response(redirect(url_for('audit_form')))
            resp.set_cookie('auth_token',username)
            resp.set_cookie('audited_by',audited)
            resp.set_cookie('logged_in','True')
            return resp
        else:
            return render_template('login.html',data='No')
    



## Ldap routings
@auth.route("/login")
def login():
    form = LoginForm()
    try:
        if 'user' in session:
            return redirect('/site/home')
        else:
            return render_template('auth/login.html', title='Sign In', form=form)
    except Exception as e:
        raise e

@auth.route('/authenticate', methods = ['POST'])
def authenticate():
    try:
        form = LoginForm()
        if request.method == 'POST':
            authenticate = False
            username = form.username.data
            password = form.password.data
            cookie = form.remember_me.data
            response, ad_grp_member, data = authenticate_user(username.lower(), password)
            print(response)
            if response == 'Success':
                if ad_grp_member:
                    matched = match_user_profile(username.lower())
                    if matched == False:
                        user_id = data.get('cn')
                        fullname = data.get('givenName')+' '+data.get('sn')
                        emp_id = data.get('employeeID')
                        email = data.get('mail')
                        # created_date = datetime.now()
                        emp_data = (user_id, fullname, emp_id, email)
                        add_user_to_db(emp_data)
                    resp = make_response(redirect('/site/home'))
                    session['user'] = username
                    if cookie is True:
                        print('Setting Cookie')
                        resp.set_cookie('userID', username, max_age= 2592000)
                    return resp
                else:
                    flash("User is not present in allowed group")
                    return redirect(url_for('auth.login'))
            else:
                flash("Invalid Credentials!")
                return redirect(url_for('auth.login'))

    except Exception as e:
        raise e



username = "raghac"
password = "*********"
response, ad_grp_member, data = authenticate_user(username.lower(), password)
#con=Connection(server,"CORP\\raghac","Lockdown@098")
print(response)
print(ad_grp_member)
print(data)