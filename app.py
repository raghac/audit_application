
# from logger import logger
from functools import wraps
from sqlalchemy import create_engine
from datetime import date
from datetime import datetime
import os
from logger import logger
from configparser import ConfigParser
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os.path
import time
import urllib.request
import json
from flask import Flask, request, render_template, url_for, redirect, session, make_response,flash
import webbrowser
import subprocess, sys
import pandas as pd
import os
from openpyxl import load_workbook
from openpyxl import Workbook
import sqlite3
from werkzeug.security import generate_password_hash, check_password_hash
import pyodbc
import math, random
from waitress import serve
# Flask constructor 
app = Flask(__name__) 
app.secret_key = 'any random string'

data=pd.DataFrame(pd.read_csv("sys_user.csv", encoding = "ISO-8859-1"))
users=data.to_dict(orient='records')

def generateOTP() :
  
    # Declare a digits variable  
    # which stores all digits 
    digits = "0123456789"
    OTP = ""
  
   # length of password can be chaged
   # by changing value in range
    for i in range(4) :
        OTP += digits[math.floor(random.random() * 10)]
  
    return OTP

def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
            session.pop('_flashes', None)
            flash('You need to login first.',category='danger')
            return redirect(url_for('index'))
    return wrap

def fetch_incident(incident): 
    try:
        logger.info('Connecting to Database using pyodbc')
        #tHIS HAS BEEN COMMENTED FOR DRIVER CHANGE. 
        #For windows driver = SQL Server Native Client 11.0
        '''conn = pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                          'Server=InfraMIDB;'
                          'Database=INFRAMI;'
                          'Trusted_Connection=yes;')'''
        #For Linux driver= ODBC Driver 17 for SQL Server                 
        conn = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};'
                          'Server=InfraMIDB;'
                          'Database=INFRAMI;'
                          'Trusted_Connection=No;'
                          'UID=APPID_AuditAPP;'
                          'PWD=a$5_c0n541tant;')

        cursor = conn.cursor()

        #engine = create_engine('sqlite:///Audit.db', echo=True)
        engine = sqlite3.connect('Audit.db')
        #sqlite_connection = engine.connect()
        #sqlite_table = "incident_audit"
        inci_id=[]
        incident_db = engine.execute('SELECT * FROM incident_audit')
        for row in incident_db:
            print(row)
            inci_id.append(row[1]) 
        print(inci_id)
        if(incident in inci_id):
            return False
        else:
            logger.info('Performing Query Operation')
            sql_query = pd.read_sql_query("SELECT * FROM INFRAMI_VIEW_Premium_SN_Incident where Incident_ID='{}'".format(incident),conn)
            #print(sql_query.to_dict(orient='record'))
            return sql_query.to_dict(orient='record')
    except Exception as e:
        logger.exception(e)
        print(str(e))
        raise 

#def check_incident(df):
    try:
        logger.info('Connecting to AUdit.db Database using sqlite')
        engine = create_engine('sqlite:///Audit.db', echo=True)
        sqlite_connection = engine.connect()
        sqlite_table = "incident_audit"
        logger.info('Inserting data ...')
        df.to_sql(sqlite_table, sqlite_connection, if_exists='replace')
        logger.info('Closing connection')
        sqlite_connection.close()
    except Exception as e:
        logger.exception(e)
        print(str(e))
        raise


def insert_auditData(df):
    try:
        logger.info('Connecting to AUdit.db Database using sqlite')
        engine = create_engine('sqlite:///Audit.db', echo=True)
        sqlite_connection = engine.connect()
        sqlite_table = "incident_audit"
        logger.info('Inserting data ...')
        df.to_sql(sqlite_table, sqlite_connection, if_exists='append')
        logger.info('Closing connection')
        sqlite_connection.close()
    except Exception as e:
        logger.exception(e)
        print(str(e))
        raise 

def send_mail(email,url,otp):
    try:
        print(email)
        email_sender = "donotreply@invesco.com"
        email_recipient= email
        msg = MIMEMultipart()
        msg['From'] = email_sender
        msg['To'] = email_recipient
        msg['Subject'] = "Password Reset"
        email_message = "Hi, Please find the below link to reset your password\n"+url+"\nOTP is: "+otp
        msg.attach(MIMEText(email_message, 'plain'))
        logger.info('establish SMTP connection...')
        print("This is otp "+otp)
        server = smtplib.SMTP("smtp.na.amvescap.com")
        #server = smtplib.SMTP('emailsmtp.app.invesco.net', 587)
        server.ehlo()
        server.starttls()
        #server.login(config.get('Email','user_id'), config.get('Email','password'))
        text=msg.as_string()
        logger.info('Sent Mail to respective user.')
        server.sendmail(email_sender, email_recipient, text)
        server.quit()
    except Exception as e:
        logger.exception(e)
        print(str(e))
        raise 
          
def update_password(password,username):
    try:
        logger.info('Encrypting user password')
        password=generate_password_hash(password, method='sha256')
        logger.info('Connecting to user database using sqlite')
        conn = sqlite3.connect('users.db')  
        conn.row_factory = sqlite3.Row
        cursor = conn.cursor()
        logger.info('Updating the user password')
        cursor.execute("UPDATE sys_users SET Password='{}'where user_name='{}'".format(password,username))
        conn.commit()
        # cursor.execute("SELECT * FROM sys_users where user_name='{}' and Password='{}'".format(username,password))
        # result = [dict(row) for row in cursor.fetchall()]
        # print(result)
        conn.close()
    except Exception as e:
        logger.exception(e)
        print(str(e))
        raise 

def check_user(corpid,password=None):
    try:
        logger.info('Connecting to user databse using sqlite')
        conn = sqlite3.connect('users.db')  
        conn.row_factory = sqlite3.Row
        cursor = conn.cursor()
        logger.info('Retrieving user info from database')
        if(password=='Invesco@123' or password==None):
            cursor.execute("SELECT * FROM sys_users where user_name='{}'".format(corpid))
            result = [dict(row) for row in cursor.fetchall()]
            print(result)
        else:
            cursor.execute("SELECT * FROM sys_users where user_name='{}'".format(corpid))
            result = [dict(row) for row in cursor.fetchall()]
            print(result)
            if(result!=[] and result[0]['Password']):
             if(check_password_hash(result[0]['Password'], password)):
                return result
            logger.info('User doesnot exists in database')
            return False
        conn.close()
        logger.info('user exists in database')
        return result
    except Exception as e:
        logger.exception(e)
        print(str(e))
        raise 
    
def append_df_to_excel(filename, df, sheet_name='Sheet1', startrow=None,
                       truncate_sheet=False):
    """
    Append a DataFrame [df] to existing Excel file [filename]
    into [sheet_name] Sheet.
    If [filename] doesn't exist, then this function will create it.

    @param filename: File path or existing ExcelWriter
                     (Example: '/path/to/file.xlsx')
    @param df: DataFrame to save to workbook
    @param sheet_name: Name of sheet which will contain DataFrame.
                       (default: 'Sheet1')
    @param startrow: upper left cell row to dump data frame.
                     Per default (startrow=None) calculate the last row
                     in the existing DF and write to the next row...
    @param truncate_sheet: truncate (remove and recreate) [sheet_name]
                           before writing DataFrame to Excel file
    @param to_excel_kwargs: arguments which will be passed to `DataFrame.to_excel()`
                            [can be a dictionary]
    @return: None

    Usage examples:

    >>> append_df_to_excel('d:/temp/test.xlsx', df)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, header=None, index=False)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, sheet_name='Sheet2',
                           index=False)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, sheet_name='Sheet2', 
                           index=False, startrow=25)

    """
    # Excel file doesn't exist - saving and exiting
    try:
        logger.info('Checking if report file exist.')
        if not os.path.isfile(filename):
            writer = pd.ExcelWriter(filename,engine='openpyxl')
            df.to_excel(
                writer,
                sheet_name=sheet_name, 
                startrow=startrow if startrow is not None else 0,index=False)
            writer.save()
            print('file')
            return
        logger.info('Opening the existing report')    
        writer = pd.ExcelWriter(filename,engine='openpyxl',mode='a')
        # try to open an existing workbook
        writer.book = load_workbook(filename)
        # get the last row in the existing Excel sheet
        # if it was not specified explicitly
        if startrow is None and sheet_name in writer.book.sheetnames:
            startrow = writer.book[sheet_name].max_row
        # truncate sheet
        if truncate_sheet and sheet_name in writer.book.sheetnames:
            # index of [sheet_name] sheet
            idx = writer.book.sheetnames.index(sheet_name)
            # remove [sheet_name]
            writer.book.remove(writer.book.worksheets[idx])
            # create an empty sheet [sheet_name] using old index
            writer.book.create_sheet(sheet_name, idx)
        # copy existing sheets
        writer.sheets = {ws.title:ws for ws in writer.book.worksheets}
        if startrow is None:
            startrow = 0
        # write out the new sheet
        logger.info('Appending record to report')  
        df.to_excel(writer,sheet_name,header=False,startrow=startrow,index=False)
        # save the workbook
        writer.save()
    except Exception as e:
        logger.exception(e)
        print(str(e))
        raise 

  # A decorator used to tell the application 
# which URL is associated function 

@app.route('/', methods =["GET","POST"])
def index():
    if request.cookies.get('auth_token'):
        session['auth_token']=request.cookies.get('auth_token')
        session['audited_by']=request.cookies.get('audited_by')
        session['logged_in']=request.cookies.get('logged_in')
        return redirect(url_for('audit_form'))
    return redirect(url_for('login'))   
    #192.168.29.164
    # return redirect('http://192.168.29.164:5000/login'),302
    
@app.route('/login',methods=['POST','GET'])
def login():
    return render_template('login.html')  
    
@app.route('/logout',methods=["GET","POST"])
def logout():
    resp=make_response(redirect(url_for('index')))
    # resp.delete_cookie('auth_token')
    resp.set_cookie('auth_token', '', expires=0)
    resp.set_cookie('audited_by', '', expires=0)
    session.pop('auth_token')
    session.pop('audited_by')
    session.pop('logged_in')
    # print(session['auth_token'])
    # session.clear()
    return resp
    
@app.route('/home',methods=["POST"])
def home_page():
    if(request.method == "POST"):
        username=request.form["corpid"]
        password=request.form['password']
        session['auth_token'] = username
        result=check_user(username,password)
        session['otp']=0
        if(result and password=='Invesco@123'):
            session['otp']=generateOTP()
            send_mail(result[0]['email'],'http://usappvisilt101:5004/set_password/'+username,session['otp'])
            return render_template('password.html',message='sent mail')  
        if(result):
            audited=result[0]['u_ad_display_name']
            session['audited_by']=audited
            resp = make_response(redirect(url_for('audit_form')))
            resp.set_cookie('auth_token',username)
            resp.set_cookie('audited_by',audited)
            resp.set_cookie('logged_in','True')
            return resp
        else:
            return render_template('login.html',data='No')
    
@app.route('/audit_form',methods=['GET','POST'])
@login_required
def audit_form():
    session.pop('_flashes', None)
    flash('Please while incident is being fetched!!',category='info')
    return render_template('home.html',message='1',session=session)
    
@app.route('/audit_dataProcess',methods=["GET","POST"])
@login_required
def audit_dataProcess():
   if(request.method == "POST"):
        incident= request.form['incident']
        engineer=request.form['engname']
        team=request.form['team']
        audited_by=request.form['audited']
        date_of_audit=request.form['auditdate']
        audit_type=request.form['auditradio']
        comments=request.form['comments']
        GAE=request.form["GAE1"]
        INC=request.form['INC1']
        #OLA=request.form['OLA1']
        #HOLD=request.form["HOLD1"]
        WOW=request.form["wow"]
        TS=request.form["TS"]
        if(audit_type=='Quality Audit'):
            CI=request.form['CI1']
            KB=request.form["KB1"]
            RES=request.form["RES1"]
            IPC='NA'
            print(CI,KB,RES)
        else:
            #IPC=request.form['IPC']
            CI='NA'
            KB='NA'
            RES='NA'
            #print(IPC)
        print(incident,engineer,team,audit_type,date_of_audit,audited_by,GAE,INC,TS) 
        # create a dataframe make function call to append to excel
        timestamp=datetime.timestamp(datetime.strptime(date_of_audit,"%m/%d/%Y"))
        df=pd.DataFrame([{"Incident":incident,'Engineer_Name':engineer,'Team_Name':team,"Audited_by":audited_by,"Date_of_Audit":date_of_audit,
        'Type_of_Audit': audit_type,'GAE':GAE,'INC':INC,'CI':CI,'KB':KB,'RES':RES,'IPC':IPC,'TS':TS,'Timestamp':timestamp,'WoW Moment': WOW,'Comments':comments}])
        print(df)
        insert_auditData(df)
        # df=df.drop(columns=['Timestamp'])
        # df = df.rename(columns={'Engineer_Name':'Engineer Name', 'Team_Name':'Team Name', 'Audited_by':'Audited by', 'Date_of_Audit':
        # 'Date of Audit', 'Type_of_Audit':'Type of Audit'},inplace=True)  
        cur_directory = os.path.dirname(os.path.abspath(__file__))
        filename=cur_directory+"\\Audit Reports\\audit_report_"+datetime.now().strftime("%B")+datetime.now().strftime("%Y")+'.xlsx'
        k=append_df_to_excel(filename,df)
        
     
   return render_template("audit_report.html",session=session)

    
@app.route('/report', methods =["GET", "POST"])
@login_required 
def audit_report():
    return render_template("home.html",data=users,session=session)     
@app.route('/mail_password',methods=["GET","POST"]) 
def mail_password():
    return render_template('password.html',message='forgot mail')
    
@app.route('/set_password/<corpid>',methods=["GET","POST"]) 
def set_password(corpid):
    if(request.method == "POST"):
        password=request.form['password']
        otp=request.form['otp']
        if otp==str(session['otp']) and corpid==session['corpid']:

        #print(corpid,password)
            update_password(password,corpid)    
            return render_template('password.html', message='set password')
        else:
            return render_template('password.html',message='incorrect otp',corpid=corpid)
            #print("Please enter correct otp!!")    
    else:
        return render_template('password.html',message='reset password',corpid=corpid)
    
@app.route('/reset_password/<corpid>',methods=["GET","POST"]) 
def reset_password(corpid):
    return render_template('password.html',message='reset password')  
    
@app.route('/forgotPassword_mail',methods=["POST"]) 
def forgotPassword_mail():
    if(request.method == "POST"):
        username=request.form['corpid']
        session['corpid']=username
        result=check_user(username)
        url='http://usappvisilt101:5004/set_password/'+username
        if(result):
            session['otp']=generateOTP()
            send_mail(result[0]['email'],url,session['otp'])
            resp = make_response(render_template("password.html", message='sent mail'))
        else:
            return render_template("password.html",message='forgot mail', data='No')
    return render_template("password.html", message='sent mail')
      
@app.route('/fetch_incident',methods=["POST","GET"])
@login_required
def getIncident():
    if(request.method == "POST"):
        inc=request.form['incident']
        result=fetch_incident(inc)
        print(type(result)) 
        if(result):
            print("inside first if!")
            session.pop('_flashes', None)
            flash('Please wait while data is processed!!',category='info')
            return render_template('home.html',message='2',data=result[0],session=session,today=date.today().strftime("%m/%d/%Y"))
        else:
            print("inside else")
            if(result==False):
                return render_template('home.html',message='1',session=session,data='yes')
            else:    
                return render_template('home.html',message='1',session=session,data='No')
        
@app.route('/management_form',methods=['POST','GET'])
def management_form():
    session.pop('_flashes', None)
    flash('Please while incident is being fetched!!',category='info')
    return render_template('Management_form.html',data=users,session=session) 

@app.route('/mgmt_formProcess',methods=["POST","GET"])    
def mgmt_formProcess():
    query_string="select * from incident_audit" 
    conn = sqlite3.connect('Audit.db')                  
    incident=request.form['incident']
    flag=False
    if(incident==''):
        if(request.form['startdate']!=''):
            
            # sdate=datetime.strptime(request.form['startdate'],"%m/%d/%Y").strftime('%Y-%m-%d')
            query_string+=" where date(datetime(Timestamp, 'unixepoch'))>='{}'".format(request.form['startdate'])
            flag=True
        if(request.form['enddate']!=''):
            if(flag):
                query_string+=" and date(datetime(Timestamp, 'unixepoch'))<='{}'".format(request.form['enddate'])
            else:
                query_string+=" where date(datetime(Timestamp, 'unixepoch'))<='{}'".format(request.form['enddate'])
        print(request.form['startdate'])
        print(query_string)
        data=pd.read_sql(query_string,conn)                 
        if(request.form['engname']!=''):
            data=data[data['Engineer_Name']==request.form['engname']]
        elif(request.form['teamname']!=''):
            data=data[data['Team_Name']==request.form['teamname']]
        if(request.form['audited']!=''):
            data=data[data['Audited_by']==request.form['audited']]
    else:
        data=pd.read_sql(query_string,conn)  
        data=data[data['Incident']==incident]
    print(data.to_dict(orient='record'))
    return render_template('index.html',data=data.to_dict(orient='record'),session=session)        
        

if __name__=='__main__':
    try:
        logger.info("Opening the Flask App")
        webbrowser.open_new('http://127.0.0.1:5000/')
        #webbrowser.open_new('http://192.168.29.164:5000/')
        #app.run() #add host name and port
        # app.run(host= '192.168.29.164')
        # webbrowser.open_new('http://localhost:5000/')
        app.run(host='0.0.0.0',port='5000')
    except Exception as e:
        logger.exception(e)
        print(str(e))
        raise 


    
    