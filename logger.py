import logging
from datetime import datetime
import os
from configparser import ConfigParser

cur_directory = os.path.dirname(os.path.abspath(__file__))
print(cur_directory)
config_filepath = os.path.join(cur_directory, 'config_file.config')
print(config_filepath)
config = ConfigParser()
config.read(config_filepath)
logger=logging.getLogger()
log_path =os.path.join(config.get('log_path','log_path'),'output_log_')
print(log_path)
logging.basicConfig(filename=log_path+datetime.now().strftime("%Y_%m_%d")+".log", 
            format='%(asctime)s %(levelname)s : %(message)s', 
            filemode='a+')
logger.setLevel(logging.INFO)