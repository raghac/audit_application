from sqlalchemy import create_engine
import pandas as pd
import sqlite3

data=pd.DataFrame(pd.read_csv('sys_user.csv'))
engine = create_engine('sqlite:///users.db', echo=True)
sqlite_connection = engine.connect()
sqlite_table = "sys_users"
data.to_sql(sqlite_table, sqlite_connection, if_exists='replace')
conn = sqlite3.connect('users.db')  

conn.row_factory = sqlite3.Row
c = conn.cursor()
c.execute("SELECT * FROM sys_users where user_name='prathrc'")
result = [dict(row) for row in c.fetchall()]
print(result)